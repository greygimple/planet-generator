# Planet Generator

<div align="center">
  <a href="https://gitlab.com/greygimple/planet-generator">
    <img src="unity-planet.png?ref_type=heads" alt="Planet Screenshot">
  </a>
</div>


> This project is no longer actively maintained.

## About The Project

Generates custom planets

- Base Shape: Planets start as a simple sphere.
- Realistic Surface: Noise patterns make the surface look bumpy and natural, like mountains and valleys.
- Colorful Biomes: Different colors are used to show areas like forests, deserts, or ice caps.
- Ocean Illusion: The ocean looks deeper in some places thanks to color changes.
- Glowing Atmosphere: A bloom effect adds a hazy glow around the planet.

## Features

- [X] Custom Sphere Mesh

- [x] Layered Noise

- [X] Biome Blending [DEPRECIATED]

## Prerequisites

- Unity Editor 2020 LTS

## Download

```bash
git clone https://gitlab.com/greygimple/planet-generator.git

cd planet-generator
```

## Contact

Want to contribute and help add more features? Fork the project and open a pull request!

If you wish to contact me, you can reach out at greygimple@gmail.com

